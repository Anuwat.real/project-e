from dash import dash, dcc, html, Input, Output
import dash_bootstrap_components as dbc
import plotly.express as px
import pandas as pd
import plotly.graph_objects as go
from dash import dcc
from dash import html
from dash.dependencies import Input, Output, State
import numpy as np
import plotly.figure_factory as ff
import dash
from pycaret.classification import *

l_n = 0

model = load_model('D:\last\project-e\model')
df = pd.read_excel('D:\projectE\project-e\data_dropout_59-64.xlsx')
df = df.rename(columns={'เกรดปี1เทอม1':'y1t1','เกรดปี1เทอม2':'y1t2','เกรดปี1เทอม3':'y1t3'
                        ,'เกรดปี2เทอม1':'y2t1','เกรดปี2เทอม2':'y2t2','เกรดปี2เทอม3':'y2t3'
                        ,'เกรดปี3เทอม1':'y3t1','เกรดปี3เทอม2':'y3t2','เกรดปี3เทอม3':'y3t3'
                        ,'เกรดปี4เทอม1':'y4t1','เกรดปี4เทอม2':'y4t2','เกรดปี4เทอม3':'y4t3'})

fig = px.pie(df, values = [50,50] ,names=['G','R'],title='student status')
figg = px.pie(
            values= [50,50],
            names = ["R","G"],
            title="Prediction"
        )
figg.update_layout(height = 400)

app = dash.Dash(external_stylesheets=[dbc.themes.BOOTSTRAP])
logo = "https://cdn-icons-png.flaticon.com/512/976/976954.png?w=1380&t=st=1678366534~exp=1678367134~hmac=6538760c26aef110cbc30c34ac4d479c9df088e997f5f422d0efc5aa99d69c52"


colors = {"background": "#111111", "text": "#7FDBFF"}
df = pd.read_excel("D:\projectE\project-e\data_dropout_59-64.xlsx")
df = df[df['GPA_SCHOOL'] <= 4]

@app.callback(
    dash.dependencies.Output('graph', 'figure'),
    [dash.dependencies.Input('input-1-1', 'value'),
     dash.dependencies.Input('input-1-2', 'value'),
     dash.dependencies.Input('input-1-3', 'value'),
     dash.dependencies.Input('input-2-1', 'value'),
     dash.dependencies.Input('input-2-2', 'value'),
     dash.dependencies.Input('input-2-3', 'value'),
     dash.dependencies.Input('input-3-1', 'value'),
     dash.dependencies.Input('input-3-2', 'value'),
     dash.dependencies.Input('input-3-3', 'value'),
     dash.dependencies.Input('input-4-1', 'value'),
     dash.dependencies.Input('input-4-2', 'value'),
     dash.dependencies.Input('input-4-3', 'value'),
     dash.dependencies.Input('submit-button', 'n_clicks')]
)
def predic_pie(input_1_1,input_1_2,input_1_3,input_2_1,input_2_2,input_2_3,input_3_1,input_3_2,input_3_3,input_4_1,input_4_2,input_4_3,n):
    global l_n
    global figg
    if n != l_n:
        l_n = n
        print("test")
        Dicts = {"y1t1":[input_1_1],
                "y1t2":[input_1_2],
                "y1t3":[input_1_3],
                "y2t1":[input_2_1],
                "y2t2":[input_2_2],
                "y2t3":[input_2_3],
                "y3t1":[input_3_1],
                "y3t2":[input_3_2],
                "y3t3":[input_3_3],
                "y4t1":[input_4_1],
                "y4t2":[input_4_2],
                "y4t3":[input_4_3],}
        data_pre = pd.DataFrame(Dicts)
        prediction = predict_model(model,data_pre)
        variable2 = prediction["prediction_label"][0]
        score =  prediction["prediction_score"][0]
        s = {"R":"G","G":"R"}
        if variable2 == "OK":
            variable2 = "G"
        figg = px.pie(
            values= [score,1-score],
            names = [variable2,s[variable2]],
            title="Prediction"
        )
        figg.update_layout(height = 400)
        return figg
    return figg

@app.callback(Output("pie-graph", "figure"), Input("year-slider", "value"))
def show_data(selected_year):
    filtered_df = df[df.ADMIT_YEAR == selected_year]

    fig = px.pie(filtered_df, names='STUDY_STATUS',title='student status',custom_data=['STUDY_STATUS'])
    fig.update_traces(hole=.4, pull=0.05, textinfo='percent+label')
    fig.update_layout(width=int(400),height=int(400), paper_bgcolor='white', plot_bgcolor='white')

    return fig

@app.callback(Output("bar-graph", "figure"), Input("year-slider", "value"))
def show_data(selected_year):
    filtered_df = df[df.ADMIT_YEAR == selected_year]

    fig1 = px.histogram(filtered_df, x="PROVINCE_NAME_THAI", title="Number of students by name",color="PROVINCE_NAME_THAI")
    fig1.update_layout(xaxis_title="Province", yaxis_title="Number of students")
    fig1.update_layout(width=int(720),height=int(625), paper_bgcolor='white', plot_bgcolor='white')

    return fig1

@app.callback(Output("line-graph", "figure"), Input("year-slider", "value"))
def show_data(v):
    filtered_df = df.groupby(['MAJOR_NAME_THAI', 'ADMIT_YEAR'])['GPA_SCHOOL'].mean().reset_index()
    fig2 = px.line(filtered_df, x='ADMIT_YEAR', y='GPA_SCHOOL', color='MAJOR_NAME_THAI', title='Average grade by dept and year')
    fig2.update_layout(width=int(1250),height=int(350), paper_bgcolor='white', plot_bgcolor='white')

    return fig2

@app.callback(Output("pie1-graph", "figure"), Input("year-slider", "value"))
def show_data(selected_year):
    filtered_df = df[df.ADMIT_YEAR == selected_year]
    dff = filtered_df[filtered_df["STUDY_STATUS"].str.contains("R")]
    fig3 = px.pie(dff, names='SEX_NAME_THAI',title='student status',custom_data=['STUDY_STATUS'],hole=0.6)
    fig3.update_layout(width=int(300),height=int(400), paper_bgcolor='white', plot_bgcolor='white')

    return fig3


graph_card_header = dbc.CardHeader("กรอกเกรดเฉลี่ยเเต่ละเทอม:")
graph_card_body = dbc.CardBody(
    [
        dbc.Input(type='text', placeholder="ปี1เทอม1", id="input-1-1", size=5),
        dbc.Input(type='text', placeholder="ปี1เทอม2", id="input-1-2", size=5),
        dbc.Input(type='text', placeholder="ปี1เทอม3", id="input-1-3", size=5),
        dbc.Input(type='text', placeholder="ปี2เทอม1", id="input-2-1", size=5),
        dbc.Input(type='text', placeholder="ปี2เทอม2", id="input-2-2", size=5),
        dbc.Input(type='text', placeholder="ปี2เทอม3", id="input-2-3", size=5),
        dbc.Input(type='text', placeholder="ปี3เทอม1", id="input-3-1", size=5),
        dbc.Input(type='text', placeholder="ปี3เทอม2", id="input-3-2", size=5),
        dbc.Input(type='text', placeholder="ปี3เทอม3", id="input-3-3", size=5),
        dbc.Input(type='text', placeholder="ปี4เทอม1", id="input-4-1", size=5),
        dbc.Input(type='text', placeholder="ปี4เทอม2", id="input-4-2", size=5),
        dbc.Input(type='text', placeholder="ปี4เทอม3", id="input-4-3", size=5),
        dbc.Button('Submit', id='submit-button', n_clicks=0),
        html.Hr(),
    ],
    id="graph-card-body"
)

# card1 = dbc.Card([card_header, card_body], className="h-100")

graph_card = dbc.Card([graph_card_header, graph_card_body], className="h-100")

app.layout = html.Div([
    dbc.NavbarSimple(
              dbc.Container(
                # Use row and col to control vertical alignment of logo / brand
                dbc.Row(
                    [
                        dbc.Col(html.Img(src=logo, height="40px")),
                        dbc.Col(dbc.NavbarBrand("", className="ms-2")),
                    ],
                    align="left",
                    className="g-0",
                ), style= {  "margin-top": "4vh"}
                ),
            ),
         dbc.Container(
        [
            html.H1("Dashbord", style={'font-size': 50, 'textAlign': 'left', 'color': '#000'}),
            html.H4("Calculate Grade Prediction", style={'font-size':20, 'textAlign': 'left', 'color': '#000'}),
            html.Hr(),
            dbc.Row(
                [
                ],style={"margin-top":'21px'}
            ),
        ],
    ),
     dbc.Container(
    [
            dbc.Row(
            [
                dbc.Col(
                    [
                        html.H4("SLIDE YEAR"),
                        html.Div([
                            dcc.Slider(
                                df["ADMIT_YEAR"].min(),
                                df["ADMIT_YEAR"].max(),
                                step=None,
                                value=df["ADMIT_YEAR"].min(),
                                marks={str(year): str(year) for year in df["ADMIT_YEAR"].unique()},
                                id="year-slider")],style={"padding-top": "20px;","margin-top":"2vh","margin-left":"2vw","margin-right":"2vw","width":"75vw",}),
                    ])
            ],
        ),
    

html.Div([dbc.Row([
    dbc.Col(
        dbc.Card(
            html.Div(
            dcc.Graph(id="pie-graph")
            )
        ),
        md=4,
        ),
        dbc.Col([
            dbc.Card(
            html.Div(
                dcc.Graph(id="pie1-graph")
            )
        ),
        ],
        md=3,
        ),
         dbc.Col(
            dbc.Card(
                html.Div( dbc.Container([
                dbc.Row(
                    
                    dcc.Graph(id="graph",figure=fig)
                         ),
                ]
            ))
    
            ),
         style={"margin-bottom":"10px"}),
        dbc.Col(
            dbc.Card(
            html.Div(
                dcc.Graph(id="bar-graph")
            ),style={"margin-top":"10px",}
        ),
        md=7),
        dbc.Col(
            dbc.Card(
                html.Div(dbc.Container([
                    dbc.Row(
                        dbc.Col(
                            dbc.Card([graph_card])
                        ),
                        justify="center"
                    )],
                    fluid=True,
                    className="mt-3",
                ))
            ),
        style={"margin-top":"10px"}),
    dbc.Row(
        dbc.Card(
            html.Div(
                [
                    html.P('GPA mean',className='font-weight-bold'),
                    dcc.Graph(id='line-graph')
                ]
            )
        ),
        style={"margin-top":"20px"}
    )
])
]
)]
     )
])
        
if __name__ == "__main__":
    app.run_server(debug=False,port = 8051)

# html.Div([dbc.Row([dbc.Col([html.Div(dcc.Graph(id="pie-graph"))]),
#                    dbc.Col([html.Div(dcc.Graph(id="bar-graph",))]),
#                    dbc.Col([html.Div(dcc.Graph(id="pie1-graph",))])
#                    ],style={'margin': '8px'}),
#                     dbc.Row([dbc.Col([html.Div([html.P('GPA mean',className='font-weight-bold'),dcc.Graph(id='line-graph')])])],
#                                 style={'margin': '8px'})
#         ]
#     )